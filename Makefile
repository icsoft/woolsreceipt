NAME = icsoft/woolsreceipt
VERSION = v0.5.0.0
BUILDTIME = `date +%FT%T%z`
GITHASH = `git rev-parse HEAD`
GITDESCRIBE = `git describe --tags`
LISTENPORT = 9090
LOGLEVEL = error

.PHONY: all compile build release clean run test ssh tag

all: compile build run

compile:
	@echo " --> Compiling ${NAME}, Version ${VERSION}"
	packr2 build -x -o image/woolsreceipt \
	-ldflags "-w -s "\
	"-X main.Version=${VERSION} "\
	"-X main.BuildTime=${BUILDTIME} "\
	"-X main.GitHash=${GITHASH} "\
	"-X main.GitDescribe=${GITDESCRIBE} "\
	"-X main.ListenPort=${LISTENPORT} "\
	"-X main.LogLevel=${LOGLEVEL}" 
	
build:
	@echo " --> Building docker container"
	docker build -t ${NAME}:${VERSION} --build-arg LISTENPORT=${LISTENPORT} --build-arg VERSION=${VERSION} --build-arg BUILDTIME=${BUILDTIME} --rm image

release:
	@echo " --> Re-tagging  docker image to latest, for upload to docker hub"
	docker tag ${NAME}:${VERSION} ${NAME}:latest
	docker push ${NAME}
	
clean:
	@echo " --> Cleaning up dangling temporary files"
	go mod tidy
	packr2 clean
	-rm ./image/woolsreceipt
	-rm ./WoolsReceipt
	-docker system prune
	-docker rm -f wools
	-docker rmi ${NAME}:${VERSION}
	-docker rmi ${NAME}:latest
	
	
test: build
	@echo " --> Running tests through SSH in container"
	env LISTENPORT=${LISTENPORT} NAME=${NAME} VERSION=${VERSION} RUNTYPE=blind ./test/runcontainer.sh

run:
	@echo " --> Running container to test with browser"
	env LISTENPORT=${LISTENPORT} NAME=${NAME} VERSION=${VERSION} RUNTYPE=web ./test/runcontainer.sh

ssh:
	@echo " --> running container with SSH access"
	env LISTENPORT=${LISTENPORT} NAME=${NAME} VERSION=${VERSION} RUNTYPE=ssh ./test/runcontainer.sh

tag:
	@echo " --> Tagging via git with version ${VERSION}"
	git tag -a ${VERSION} -m "release ${VERSION}"

	