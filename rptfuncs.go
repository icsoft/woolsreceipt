// General functions
package main

import (
	"fmt"
	"strconv"
	"strings"
)

// grandTotal returns the total dollar value of a list of products
func grandTotal(items []ProductItem) string {
	gt := 0

	for _, v := range items {
		tot := v.Amount * v.UnitPrice
		if v.AmtGrams {
			tot = tot / 1000
		}
		gt += tot
	}

	return IntToDollarString(gt)
}

//lineTotal returns the total dollar value of one line item
func lineTotal(amount int, unitPrice int, amtGrams bool) string {
	tot := amount * unitPrice
	if amtGrams {
		tot = tot / 1000
	}

	return IntToDollarString(tot)
}

//decAmt returns amount converted to a string value or Kg string value
func decAmt(amount int, decGrams bool) string {
	if decGrams {
		return fmt.Sprintf("%.3f", (float64(amount) / 1000.00))
	} else {
		return strconv.Itoa(amount)
	}
}

// DollarParse converts a sting value dollar amount with decimal place to an inter of the amount in cents
// ie. $123.45 returns 12345, $0.00 returns 0, $5 returns 500.
func DollarParse(s string) (int, error) {
	retVal := 0
	var err error

	if strings.ContainsRune(s, '$') {
		s = strings.TrimPrefix(s, "$")
	}

	// Sanity check
	fl, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return 0, fmt.Errorf("Unable to parse to a float, %s", err.Error())
	}
	n := int((fl * 100) + 0.5)

	if strings.ContainsRune(s, '.') {
		s = strings.Replace(s, ".", "", 1)
		retVal, err = strconv.Atoi(s)
		if err != nil {
			return 0, err
		}
	} else {
		retVal, err = strconv.Atoi(s)
		if err != nil {
			return 0, err
		}
		retVal = retVal * 10
	}

	if retVal == n {
		return retVal, nil
	} else {
		return 0, fmt.Errorf("Invalid dollar conversion, expected %d, got %d", n, retVal)
	}
}

// IntToDollarString is the inverse function to DolarStringToInt and returns a formatted dollar amount.
func IntToDollarString(amtInCents int) string {
	return fmt.Sprintf("$%.2f", (float64(amtInCents) / 100.00))
}

// decMinStep returns the minimum step value for altering a Kg amount,
// or an integer unit amount for a given line item.
func decMinStep(decGrams bool) string {
	if decGrams {
		return fmt.Sprintf("0.001")
	} else {
		return fmt.Sprintf("1")
	}
}
