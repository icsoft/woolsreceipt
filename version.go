package main

import (
	"fmt"
)

// Updated at build time from Makefile
var (
	Version     = "Debug"
	BuildTime   = "Not Set"
	GitHash     = "Not Set"
	GitDescribe = "Not Set"
)

func versionInfo() string {
	return fmt.Sprintf("Version: %s\nBuild Time: %s\nGit Hash: %s\nGit Description: %s\n", Version, BuildTime, GitHash, GitDescribe)
}
