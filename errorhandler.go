//This is the custom error page
package main

import (
	"fmt"
	"log"
	"net/http"
	"text/template"
)

type ErrorPage struct {
	ErrorString      string
	HTMLErrorCode    string
	HTMLErrorNo      string
	SuggestedUrl     string
	SuggestedUrlName string
	RFCErrorNo       int
	AdditionalInfo   string
}

// HTTP status codes as registered with IANA.
// See: http://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
func rfcErrorHandler(w http.ResponseWriter, r *http.Request, status int) {
	ep := new(ErrorPage)
	ep.SuggestedUrl = "/"
	ep.SuggestedUrlName = "our Home Page"
	ep.RFCErrorNo = status

	if status == http.StatusNotFound {
		ep.ErrorString = "Sorry, the page you are looking for has not been found."
	}
	errorHandler(w, r, ep)
}

func customErrorHandler(w http.ResponseWriter, r *http.Request, errorString string) {
	customErrorHandlerWithPage(w, r, errorString, "our Home Page", "/")
}

func customErrErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	customErrorHandler(w, r, err.Error())
}

func customErrorHandlerWithPage(w http.ResponseWriter, r *http.Request, errorString string, suggestedPage string, suggestedUrl string) {
	ep := new(ErrorPage)
	ep.RFCErrorNo = http.StatusInternalServerError
	ep.SuggestedUrl = suggestedUrl
	ep.SuggestedUrlName = suggestedPage
	ep.ErrorString = errorString

	errorHandler(w, r, ep)
}

func genericErrorHandler(w http.ResponseWriter, r *http.Request) {
	customErrorHandler(w, r, "An unexpected error has occurred.")
}

func errorHandler(w http.ResponseWriter, r *http.Request, ep *ErrorPage) {
	w.WriteHeader(ep.RFCErrorNo)
	ep.HTMLErrorCode = http.StatusText(ep.RFCErrorNo)
	ep.HTMLErrorNo = fmt.Sprint(ep.RFCErrorNo)
	ep.AdditionalInfo = "The server has not provided any additional information about this error."
	t, err := template.ParseFiles("HTMLTemplates/errorpage.html.gtpl")
	if err != nil {
		fmt.Println("Error parsing error: ", err)
		fmt.Println("Error Struct: ", ep)
		log.Fatal(err)
	}
	err = t.Execute(w, ep)
	if err != nil {
		fmt.Println("Error executing error: ", err)
		fmt.Println("Error Struct: ", ep)
		log.Fatal(err)
	}
	fmt.Println("Error Struct: ", ep)
}
