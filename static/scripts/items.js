
function fillJSON( ){
	var xhr = new XMLHttpRequest();
	xhr.addEventListener("loadend", reqListener);
	xhr.open("POST", "/receiptpdf", true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify(tableToObj(document.getElementsByTagName('table')[0])));
};

function reqListener(e){	
	//redirect
	window.location = this.responseText;	
};

var tableToObj = function( table ) {
    var trs = table.rows,
        i = 0,
        j = 0,
        keys = [],
        obj, outerobj, ret = [];

    for (; i < trs.length; i++) {
        if (i == 0) {
        		//Use the th headers as JSON keys 
            for (; j < trs[i].children.length - 1; j++) {
                 keys.push(trs[i].children[j].innerHTML.toLowerCase().replace('/',''));
            }
            //add amtgrams key
            keys.push("amtgrams");
        } else {
            obj = {};
            // only encode items that have been ticked
			if ( trs[i].children[3].children[0].checked ) {
	            //Description field
				obj[keys[0]] = trs[i].children[0].innerHTML;
				//Price/Unit fields
				obj[keys[1]] = parseInt(trs[i].children[1].children[0].value);
				//Units fields (maybe in grams)
				if (trs[i].children[2].children[1].value == 'true') {
					var x = parseFloat(trs[i].children[2].children[0].value);
					obj[keys[2]] = (x * 1000);
				} else {
					obj[keys[2]] = parseInt(trs[i].children[2].children[0].value);
				}
				//amtgrams field
				obj[keys[3]] = trs[i].children[2].children[1].value;
				ret.push(obj);
			}
        }
    }
	outerobj = {};
	outerobj["items"] = ret;
	outerobj["receiptdate"] = rd;
	outerobj["invoiceno"] = fn;
	
    return outerobj;
};

function addItem( ) {
    var table = document.getElementById("items");
	
	//get the new data and clear values
	var row = table.rows[table.rows.length - 1];
	if (row.children[0].children[0].value == ""){
		return
	} 
	var description = row.children[0].children[0].value;
	row.children[0].children[0].value = "";
	var priceUnit = row.children[1].children[0].value;
	row.children[1].children[0].value = "0.00"
	var amount = row.children[2].children[0].value;
	row.children[2].children[0].value = "1"
	
	//insert the data
    var newRow = table.insertRow(table.rows.length - 1);
		
    var cell = newRow.insertCell(0);
	cell.innerHTML = description;
	
	cell = newRow.insertCell(1);
	cell.innerHTML = "$" + priceUnit + "<input type='hidden' value='" + (priceUnit * 100) + "'>";
	
	cell = newRow.insertCell(2);
	cell.className = 'units';
	cell.innerHTML = "<input type='number' name='amount' min='1' value='" + amount + "' id='amount'><input type='hidden' value='false'>";
	
	cell = newRow.insertCell(3);
	cell.className = 'tick';
	cell.innerHTML = "<input type='checkbox' name='selected'>";
		
};
