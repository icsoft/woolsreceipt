package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/gobuffalo/packr/v2"
	log "github.com/sirupsen/logrus"
)

const sharedLocation = "usergenerated/fromwools/"

type FileList struct {
	FileCount   string
	VersionInfo string
	Files       []OsFile
}

type OsFile struct {
	Name string
	Date string
}

// Set LogLevel in Makefile, usually to error, defaults to error if incorrect in Makefile
// Valid levels are "panic, fatal, error, warn, info, debug, trace"
var LogLevel = "debug"

// Set ListenPort in Makefile
var ListenPort = "8080"

var templatebox *packr.Box

func init() {
	log.SetOutput(os.Stdout)
	loglevel, err := log.ParseLevel(LogLevel)
	if err != nil {
		loglevel = log.ErrorLevel
	}
	log.SetLevel(loglevel)
	log.SetReportCaller(LogLevel == "debug")
}

func main() {
	log.Debug("Icsoft WoolsReceipt, ", versionInfo())

	//start a web server
	http.HandleFunc("/", homePage)
	http.HandleFunc("/receipt", createTestReceipt)
	http.HandleFunc("/receiptpdf", createReceiptPdf)
	http.HandleFunc("/itemselect", itemSelection)
	http.HandleFunc("/upload", uploadFile)

	templatebox = packr.New("templatebox", "./HTMLTemplates")
	staticbox := packr.New("staticbox", "./static")

	fs := http.FileServer(staticbox)
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	fs1 := http.FileServer(http.Dir(filepath.Join(pwd(), "usergenerated")))
	http.Handle("/usergenerated/", http.StripPrefix("/usergenerated/", fs1))

	log.Debug("Web server starting on port ", ListenPort)
	log.Fatal(http.ListenAndServe(":"+ListenPort, nil)) // set listen port
}

// homePage lists the PDF receipts to choose from
// then imports it and displays any errors
func homePage(w http.ResponseWriter, r *http.Request) {

	//	if r.URL.Path != "/" { //have they tried for a page thats not there?
	//		rfcErrorHandler(w, r, http.StatusNotFound)
	//		return
	//	}

	pdfs, err := getFilelist(filepath.Join(pwd(), sharedLocation))
	if err != nil {
		log.WithError(err).Error("Getting file list")
	}

	html, err := templatebox.FindString("fileselect.html.gtpl")
	if err != nil {
		log.WithError(err).Error("finding template")
	}

	t, err := template.New("t1").Parse(html)
	if err != nil {
		log.WithError(err).Error("parsing template")
	}

	pdfs.VersionInfo = versionInfo()
	err = t.Execute(w, pdfs)

	if err != nil {
		log.WithError(err).Error("executing template")
	}
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)

	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		log.WithError(err).Warn("Error Retrieving the File")
		return
	}
	defer file.Close()

	contextLogger := log.WithFields(log.Fields{
		"Uploaded File": handler.Filename,
		"File Size":     handler.Size,
		"MIME Header":   handler.Header,
	})
	contextLogger.Debug("File info")

	// Create a temporary file within our directory that follows
	// a particular naming pattern
	tempPath := filepath.Join(pwd(), sharedLocation)
	tempFile, err := ioutil.TempFile(tempPath, "upload-*.pdf")
	if err != nil {
		contextLogger.WithError(err).Error("Creating temp file in ", tempPath)
		homePage(w, r)
		return
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		contextLogger.WithError(err).Error("Reading byte array")
		homePage(w, r)
		return
	}
	// write this byte array to our temporary file
	_, err = tempFile.Write(fileBytes)
	if err != nil {
		contextLogger.WithError(err).Error("Writing byte array to file")
		homePage(w, r)
		return
	}
	contextLogger.Debug("Successfully Uploaded File")
	homePage(w, r)
}

func createTestReceipt(w http.ResponseWriter, r *http.Request) {
	itemData := ItemTestData()

	html, err := templatebox.FindString("woolsreceipt.html.gtpl")
	if err != nil {
		log.WithError(err).Error("Loading template")
	}

	funcMap := template.FuncMap{
		"decAmt":       decAmt,
		"dollarString": IntToDollarString,
		"grandTotal":   grandTotal,
		"lineTotal":    lineTotal,
	}

	//t := template.Must(template.New("root").Funcs(funcMap).Parse(html))
	t, err := template.New("root").Funcs(funcMap).Parse(html)
	if err != nil {
		log.WithError(err).Error("Parsing template")
	}

	err = t.Execute(w, itemData)
	if err != nil {
		log.WithError(err).Error("Executing template")
	}
}

func createReceiptPdf(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		http.Redirect(w, r, "/", 301)
	}
	defer r.Body.Close()

	body, _ := ioutil.ReadAll(r.Body)
	log.WithField("bodytext", string(body)).Debug("Reading body text")

	//get info from form
	itemData := new(ItemsTable)
	err := json.Unmarshal(body, &itemData)
	if err != nil {
		log.WithField("bodytext", string(body)).WithError(err).Error("Unmarshalling JSON")
		//rfcErrorHandler(w, r, http.StatusUnprocessableEntity)
		return
	}

	//TODO validate item data

	fileWithPath := filepath.Join(pwd(), sharedLocation, itemData.InvoiceNo) + ".html"
	f, err := os.Create(fileWithPath)
	if err != nil {
		log.WithError(err).Error("Creating file: ", fileWithPath)
		return
	}

	funcMap := template.FuncMap{
		"decAmt":       decAmt,
		"dollarString": IntToDollarString,
		"grandTotal":   grandTotal,
		"lineTotal":    lineTotal,
	}

	html, err := templatebox.FindString("woolsreceipt.html.gtpl")
	if err != nil {
		log.WithError(err).Error("Cant find woolsreceipt.html.gtpl")
	}

	t := template.Must(template.New("root").Funcs(funcMap).Parse(html))

	err = t.Execute(f, itemData)
	if err != nil {
		log.WithError(err).Error("Executing woolsreceipt template")
	}

	pdfFile, err := receiptToPdf(itemData.InvoiceNo)
	if err != nil {
		log.WithError(err).Error("converting receipt html to PDF")
	}
	w.Write([]byte(pdfFile))

}

// itemSelection allows selection of one or more
// items to choose from to create your receipt
func itemSelection(w http.ResponseWriter, r *http.Request) {

	pdf := "" //the pdf file selected to convert

	//Get the file name of the chosen file
	if r.Method == "GET" {
		http.Redirect(w, r, "/", 301)
		return
	} else {
		r.ParseForm()
		if r.Form["refresh"] != nil {
			log.Debug("Refresh")
			homePage(w, r)
			return
		}
		for k := range r.Form {
			pdf = k
		}
	}

	// Are we trying to delete a file?
	if strings.HasPrefix(pdf, "delete") {
		pdf = strings.TrimPrefix(pdf, "delete")
		log.Debug("File deletion selected for file: ", pdf)

		fileToRemove := filepath.Join(pwd(), sharedLocation, pdf) + ".pdf"
		err := os.Remove(fileToRemove)
		if err != nil {
			log.WithError(err).Error("Deleting file: ", fileToRemove)
		}
		homePage(w, r)
		return
	}

	convertedFile, err := convertPdf(pdf + ".pdf")
	//_, err := convertPdf(pdf + ".pdf")
	if err != nil {
		customErrErrorHandler(w, r, err)
		return
	}

	itemData, err := ItemImportFileData(convertedFile)
	if err != nil {
		//customErrErrorHandler(w, r, err)
		log.WithError(err).Error("Attempting to import data")
		return
	}

	funcMap := template.FuncMap{
		"decAmt":       decAmt,
		"decMinStep":   decMinStep,
		"dollarString": IntToDollarString,
		"grandTotal":   grandTotal,
	}

	html, err := templatebox.FindString("itemselect.html.gtpl")
	if err != nil {
		log.WithError(err).Error("")
	}

	t := template.Must(template.New("root").Funcs(funcMap).Parse(html))

	err = t.Execute(w, itemData)
	if err != nil {
		log.WithError(err).Error("Executing itemselect template")
	}
}

// convertPdf converts the PDF file from wools online to a text file
func convertPdf(filename string) (string, error) {
	var c *exec.Cmd

	//pdftotext -layout woolworths2.pdf 20.txt
	fileWithPath := filepath.Join(pwd(), sharedLocation, filename)
	fileWithPathExtension := fileWithPath + ".txt"
	c = exec.Command("pdftotext", "-layout", fileWithPath, fileWithPathExtension)

	c.Stdout = os.Stdout
	err := c.Run()
	if err != nil {
		log.WithError(err).Error("Converting PDF to text")
		return "", err
	}
	return fileWithPathExtension, nil
}

// receiptToPdf converts your finished receipt to a PDF file
// where fileName is the unique identifier with no file extension
func receiptToPdf(fileName string) (string, error) {
	var c *exec.Cmd

	//wkhtmltopdf page http://myurl.com.au/receipt.html mumsreceipt.pdf
	htmlFilePath := filepath.Join(pwd(), sharedLocation, fileName) + ".html"
	pdfRelativeFilePath := filepath.Join("/usergenerated/exported/", fileName) + ".pdf"

	log.Debug("Converting: ", htmlFilePath, " To: ", pwd(), pdfRelativeFilePath)
	c = exec.Command("wkhtmltopdf", "page", htmlFilePath, pwd()+pdfRelativeFilePath)

	c.Stdout = os.Stdout
	err := c.Run()
	if err != nil {
		log.WithError(err).Error("Converting receipt to PDF")
		return "", err
	}
	return pdfRelativeFilePath, nil
}

//getFilelist gets the file list for a specified local location
// FML supplanted by filepath.glob
func getFilelist(location string) (*FileList, error) {

	fl := new(FileList)
	var fCount int

	err := filepath.Walk(location, func(path string, f os.FileInfo, err error) error {
		if f == nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		if strings.HasSuffix(path, ".pdf") {
			fileName := strings.TrimSuffix(f.Name(), ".pdf")
			file := OsFile{fileName, f.ModTime().Format("02 Jan, 2006")}
			fl.Files = append(fl.Files, file)
			fCount++
		}
		return nil
	})
	fl.FileCount = fmt.Sprint(fCount)

	if err != nil {
		return fl, err
	}

	return fl, nil
}

//The current working directory of the app without trailing directory seperator
func pwd() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return ""
	}
	return dir
}
