module icsoft/WoolsReceipt

go 1.12

require (
	github.com/gobuffalo/packr/v2 v2.0.7
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24
	github.com/sirupsen/logrus v1.4.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a // indirect
	golang.org/x/sys v0.0.0-20190318195719-6c81ef8f67ca // indirect
	golang.org/x/tools v0.0.0-20190319232107-3f1ed9edd1b4 // indirect
)
