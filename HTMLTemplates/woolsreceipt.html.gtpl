<!DOCTYPE html>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Icsoft, Software Development" />
		<meta name="author" content="Matthew Hooper" />
		<meta name="keywords" content="Icsoft, Software, Programming, Computers, Support" />
		
		<title>WoolsReceipt - Receipt</title>
		
		<style>
		body{
				font-family: Arial, Helvetica, sans-serif;
				background-color: white;
				overflow: auto;
			}
			td, th {
			    border: 1px solid #ddd;
			    padding: 8px;
			}
			th {
			    padding-top: 12px;
			    padding-bottom: 12px;
			    text-align: left;
			    background-color: #4CAF50;
			    color: white;
			}
			table {
				width:100%;
			}
			#main{
				color: black;
				margin-left: auto;
				margin-right: auto;
				width: 733px;
				height: auto;
				position: relative;
			}
			#head{
				height: 70px;
				font-weight: bold;
			}
			#title{
				text-align: center;
				width: 70%;
				float: left;
				font-size: 1.5em;
				text-decoration: underline;
			}
			#date{
				width: 30%;
				text-align: right;
				float: right;
			}
			#grandtotal{
				width: 30%;
				text-align: right;
				float: right;
				margin-right: 20px;
			}
			#footer{
				padding-top: 70px;
				font-style: italic;
			}
		</style>
	</head>
	<body>
		<div id="main">
			<div id="head">
				<div id="title">Shopping</div>
				<div id="date">
					{{.ReceiptDate}}<br>
					Invoice: {{.InvoiceNo}}</div>
			</div>
			<div id="itemsdiv">
				 <table id="items">
					<tr>
						<th>Amt</th>
						<th>Description</th>
						<th>Price/Unit</th>
						<th>Total</th>
					</tr>
					{{range .Items}}
					<tr>
						<td>{{decAmt .Amount .AmtGrams}}{{ if .AmtGrams }}Kg{{ end }}</td>
						<td>{{.Description}}</td>
						<td>{{dollarString .UnitPrice}}{{ if .AmtGrams }}/Kg{{ end }}</td>
						<td>{{lineTotal .Amount .UnitPrice .AmtGrams}}</td>
					</tr>
					{{end}}
				</table> 
			</div>
			<div id="grandtotal">
				<hr>
				Grand Total {{grandTotal .Items}}
			</div>
			<div id="footer">
				NOTE: * indicates GST in the item
			</div>
		</div>
	</body>
</html>
