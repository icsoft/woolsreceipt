#!/bin/bash

#---------------------------------------------
#
# Runs the wkhtmltopdf utility headlessly.
#
# xvfb-run is a xserver virtual frame buffer.
#
#---------------------------------------------

xvfb-run -a -s "-screen 0 640x480x16" wkhtmltopdf_matt "$@"
