# WoolsReceipt

## Summary
WoolsReceipt is a receipt creation application that scrapes product data from your receipt pdf that you download from woolworths online and allows for the user selection of line items to create a secondary receipt, ie. it splits your bill.
This is specifically for people that do one order for multiple people to save the delivery fee and want to split the receipt up.

## Docker
#### Precompiled image
This programme is available from docker hub as a precompiled image:

    docker pull icsoft/woolsreceipt:latest

#### Running the image
    docker run -d --name wools -p 9090:9090 icsoft/woolsreceipt:latest

After the container has been started, use http://localhost:9090 as the URL to see the app.	

## Notes
### Requires dependancies
These dependancies are part of the docker image 
* xvfb
* libfontconfig1
* wkhtmltopdf
* poppler-utils

### Settings
No current config file, hardcoded settings:
* Port 9090 set on build

### Building from source

#### Compile the executable and build the docker image
    requires packr2 to build
    git pull
    cd  woolsreceipt
    make compile
    make build
	
#### Compile the executable only
    make compile

### To Do
Consider this Beta quality code as it is hardly robust.
* Clean up code
	* Inconsistant error handling
	* Split off functionality
* Add tests
* Add settings through redis
	* Port number
	* Debug option
	* Logging server
* Add input validation and sanity checks
* Add logging and debug code
